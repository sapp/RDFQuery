#!/bin/bash
#============================================================================
#title          :SAPP_RDFQuery
#description    :RDF Query module using HDT
#author         :Jasper Koehorst
#date           :2016
#version        :0.0.1
#============================================================================

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


# MAC
brew install raptor
brew install serd

# LINUX
sudo apt-get install -y libraptor2-dev
sudo apt-get install -y libserd-dev

# GIT
git clone https://github.com/rdfhdt/hdt-cpp.git
cd hdt-cpp/hdt-lib/ && make && cd ../../

git clone https://github.com/jjkoehorst/hdt-java.git

#MAVEN
mvn install -f "$DIR/hdt-java/pom.xml" --quiet

cp "$DIR/hdt-java/hdt-jena/target/hdt-jena-2.0-jar-with-dependencies.jar" "$DIR/"

# HDT test creation
cp "$DIR/hdt-cpp/hdt-lib/tools/rdf2hdt" "$DIR/"

$DIR/rdf2hdt -i -f rdfxml $DIR/test_dir/input.rdf $DIR/test_dir/output.dat

java -jar $DIR/hdt-jena-2.0-jar-with-dependencies.jar $DIR/test_dir/output.dat "SELECT * WHERE { ?S ?P ?O } LIMIT 10" > $DIR/test_dir/result.csv


